import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.util.Objects.isNull;

@SuppressWarnings("unchecked")
public class CustomHashMap<K, V> implements Map<K, V> {
    private static final int INITIAL_SIZE = 16;
    private static final float INITIAL_LOAD_FACTOR = 0.75f;

    private int realSize;
    private int size;
    private float loadFactor;
    private Node[] nodes;

    public CustomHashMap() {
        size = 0;
        realSize = INITIAL_SIZE;
        loadFactor = INITIAL_LOAD_FACTOR;
        nodes = new Node[realSize];
    }

    public CustomHashMap(int size) {
        if (size < 0) throw new IllegalArgumentException();

        this.size = size;
        realSize = size;
        loadFactor = INITIAL_LOAD_FACTOR;
        nodes = new Node[size];
    }

    public CustomHashMap(int size, float loadFactor) {
        if (size < 0 || loadFactor <= 0) throw new IllegalArgumentException();

        this.size = size;
        realSize = size;
        this.loadFactor = loadFactor;
        nodes = new Node[size];
    }

    public CustomHashMap(Map<? extends K, ? extends V> m) {
        this();
        if (isNull(m)) throw new IllegalArgumentException();
        putAll(m);
    }

    public int hash(K key) {
        return key.hashCode() & (realSize - 1);
    }

    public void resize(Node[] nodes) {
        if (++size == realSize && nodes == this.nodes) {
            realSize = (int) Math.ceil((realSize + realSize * loadFactor));
            Node[] newNodes = new Node[realSize];
            keySet().forEach(key -> putNodes(key, get(key), newNodes));
            this.nodes = newNodes;
        }
    }

    private class Node<K, V> {
        private K key;
        private V value;
        private Node next;

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
            next = null;
        }

        @Override
        public String toString() {
            return String.format("%s %s next: |%s|", key, value, next);
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean containsKey(Object obj) {
        for (Node node : nodes) {
            if (!isNull(node)) {
                if (node.key.equals(obj)) {
                    return true;
                }


                while (!isNull(node.next)) {
                    node = node.next;

                    if (node.key.equals(obj)) {
                        return true;
                    }
                }

            }
        }

        return false;
    }

    @Override
    public boolean containsValue(Object obj) {
        for (Node node : nodes) {
            if (!isNull(node)) {
                if (node.value == obj) {
                    return true;
                }


                while (!isNull(node.next)) {
                    node = node.next;

                    if (node.value == obj) {
                        return true;
                    }
                }

            }
        }

        return false;
    }

    @Override
    public V get(Object obj) {
        int nodeIndex = 0;
        while (nodeIndex < nodes.length) {
            if (!isNull(nodes[nodeIndex])) {
                if (nodes[nodeIndex].key.equals(obj)) {
                    return (V) nodes[nodeIndex].value;
                }

                while (!isNull(nodes[nodeIndex].next)) {
                    nodes[nodeIndex] = nodes[nodeIndex].next;

                    if (nodes[nodeIndex].key.equals(obj)) {
                        return (V) nodes[nodeIndex].value;
                    }
                }
            }

            nodeIndex++;
        }

        return null;
    }

    private V putNodes(K key, V value, Node[] nodes) {
        int keyIndex = hash(key);

        if (!isNull(nodes[keyIndex])) {
            V previousValue;

            if (nodes[keyIndex].key.equals(key)) {
                previousValue = (V) nodes[keyIndex].value;
                nodes[keyIndex].value = value;
                return previousValue;
            } else {
                Node currentNode = nodes[keyIndex];
                while (!isNull(currentNode.next)) {
                    if (currentNode.key.equals(key)) {
                        previousValue = (V) currentNode.value;
                        currentNode.value = value;
                        return previousValue;
                    }
                    currentNode = currentNode.next;
                }

                currentNode.next = new Node(key, value);
                resize(nodes);
                return null;
            }
        } else {
            nodes[keyIndex] = new Node(key, value);
            resize(nodes);
            return null;
        }
    }

    @Override
    public V put(K key, V value) {
        return putNodes(key, value, nodes);
    }

    @Override
    public V remove(Object obj) {
        K key = (K) obj;
        int keyIndex = hash(key);

        if (!isNull(nodes[keyIndex])) {
            V previousValue;

            if (nodes[keyIndex].key.equals(key)) {
                previousValue = (V) nodes[keyIndex].value;
                nodes[keyIndex] = nodes[keyIndex].next;
                size--;
                return previousValue;
            } else {
                Node currentNode = nodes[keyIndex];
                Node previousNode = nodes[keyIndex];
                while (!isNull(currentNode)) {
                    if (currentNode.key.equals(key)) {
                        previousValue = (V) currentNode.value;
                        previousNode.next = currentNode.next;
                        size--;
                        return previousValue;
                    }
                    previousNode = currentNode;
                    currentNode = currentNode.next;
                }
            }
        }

        return null;
    }

    public void putAll(Map<? extends K, ? extends  V> m) {
        if (isNull(m)) throw new IllegalArgumentException();
        m.keySet().forEach(key -> put(key, m.get(key)));
    }

    @Override
    public void clear() {
        if (size > 0) {
            Arrays.fill(nodes, null);

            size = 0;
        }
    }

    @Override
    public Set<K> keySet() {
        if (size > 0) {
            Set<K> hashSet = new HashSet<>();

            for (Node node : nodes) {
                if (!isNull(node)) {
                    hashSet.add((K) node.key);

                    while (!isNull(node.next)) {
                        node = node.next;
                        hashSet.add((K) node.key);
                    }
                }
            }

            return hashSet;
        }

        return new HashSet<K>();
    }

    @Override
    public Collection<V> values() {
        if (size > 0) {
            List<V> arrayList = new ArrayList<>();

            for (Node node : nodes) {
                if (!isNull(node)) {
                    arrayList.add((V) node.value);

                    while (!isNull(node.next)) {
                        node = node.next;
                        arrayList.add((V) node.value);
                    }
                }
            }

            return arrayList;
        }

        return new ArrayList<>();
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        if (size > 0) {
            Set<Entry<K, V>> entryHashSetHashSet = new HashSet<>();

            for (Node node : nodes) {
                if (!isNull(node)) {
                    entryHashSetHashSet.add(new AbstractMap.SimpleEntry(node.key, node.value));

                    while (!isNull(node.next)) {
                        node = node.next;
                        entryHashSetHashSet.add(new AbstractMap.SimpleEntry(node.key, node.value));
                    }
                }
            }

            return entryHashSetHashSet;
        }

        return new HashSet<>();
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("[");
        for (Node node : nodes) {
            if (!isNull(node)) {
                result.append(node).append(" ");
            }
        }
        return result.append("]").toString();
    }
}
