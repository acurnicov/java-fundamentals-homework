import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

@Getter
@EqualsAndHashCode(exclude = {"details"})
public class Student {
    private String name;
    private LocalDate dateOfBirth;
    private String details;

    public Student(String name, LocalDate dateOfBirth, String details) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.details = details;
    }

    @Override
    public String toString() {
        String formattedDate = dateOfBirth.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG));
        return String.format("%s %s %d", name, formattedDate, hashCode());
    }
}